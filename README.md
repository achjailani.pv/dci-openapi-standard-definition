# DCI OpenAPI Standard Definition
[![Generic badge](https://img.shields.io/badge/OAS-3.0.1-blue.svg)](https://shields.io/)  
OpenAPI Standard Definition for DCI Applications.

## Introduction
DCI OpenAPI Standard Definition defines a standard definition to write an API Documentation for DCI Applications. This standard is based on Open API Specification (OAS) in version ```3.0.1```, other versions are not allowed.

Reference: [OpenAPI Spec](https://swagger.io/specification)
## Standard Definition
  * [Openapi](#openapi)
  * [Info](#info)
  * [Servers](#servers)
  * [Tags](#tags)
  * [Paths](#paths)
    * [Security](#security)
    * [Parameters](#parameters)
    * [RequestBody](#requestbody)
      * [Content](#content)
    * [Responses](#responses)
  * [Components](#components)
    * [Schemas](#schemas)
  * [Examples](#examples)

The full DCI OpenAPI Standard Definition example you can find [here](openapi.yaml)

## Openapi
An OpenAPI Specification version. The format should follow the [semantic version number](https://swagger.io/specification/#versions). however, DCI OpenAPI Standard Definition uses fixed version number which is ```3.0.1```, it means everytime writing an OpenAPI should use that version.

```yaml
openapi: 3.0.1
```

## Info
Providing metadata about the API, the following attributes are required
| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| title         | string   | A title of the API |
| description   | string   | A description detail of the API |
| contact       | object   | A contact information for the API. contains 3 attributes, `name`, `url`, and `email` |
| version       | number   | A version of the API |

```yaml
info:
  title: DCI OpenAPI Standard Definition
  description: This document is an example of DCI OpenAPI Standard Definition
  contact:
    name: DCI Developer
    url: https://developer.dcidev.id/support
    email: developer@dcidev.id
  version: 1.0.0
```

## Servers
A list of servers used of the API. Tt provides connectivity information to a terget server. The following should be exist.

| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| url           | string   | A url of targer server |
| description   | string   | A description detail of target server |

```yaml
servers:
  - url: https://stg-api.glacier.dcidev.id/v1
    description: The Staging API Server
  - url: https://api.glacier.dcidev.id/v1
    description: The Production API Server
```

## Tags
Tags are used to group the path operations. Each tag name mus be unique. the following attributes are required.

| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| name          | string   | A tag name, should be unique |
| description   | string   | A description detail of the tag name |

```yaml
tags:
  - name: Auth
    x-displayName: Authentication Process
    description: All authentication process required
  - name: Pet
    x-displayName: Pet Process
    description: All Pet process required
```

## Paths
A list of paths and each operations for the API. Every path should have HTTP Method definition which is called path item object, for example: `GET`, `POST`, `PUT` etc. A path item object should have the following attributes.

| Attributes    | Type     | Required | Description  |
| :------------ |:--------:|:--------:| :------------|
| tags          | array    | true     | A lisf of tag names |
| summary       | string   | true     | A short description of the path item object |
| description   | string   | true     | A description detail of the path item object |
| operationId   | string   | true     | A unique identifier of the path item object |
| security      | array    | false    | array object of the path item object
| parameters    | array    | false    | A list of parameters need by the path item object |
| requestBody   | object   | false    | A request body object |
| responses     | object   | true     | An information response of the  path item object. see [Path Response](#responses) |

```yaml
paths:
  /signin:
    post: 
      tags:
        - Auth
      summary: Sign in summary
      description: Sign in description
      operationId: signin
      requestBody:
        required: true
        description: Sign description
        content:
          application/json:
            schema: 
              $ref: '#/components/schemas/SignIn'
            examples:
              Valid:
                value:
                  username: HM58012^
                  password: Xsh^76909080
              Invalid:
                value:
                  username: HM5
                  password: Xsh
      responses:
        200:
          description: Login successful
          content:
            application/json:
              schema:
                type: object
```

### Security
Defines a security schema that can be used by the API operations

The following attributes are required.
| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| type          | string   | The type of the security schema, valid values are `apiKey`, `http`, `oauth2`, and `openIdConnect` |
| description   | string   | A description detail of the security schema |
| schema        | string   | The name of the HTTP Authorization schema to be used in the Authorization Header. the valid value is `http`|

Example of security schema component
```yaml
components:
  securitySchemes:
    BearerAuth: 
      type: http
      description: JWT Authorization header using the Bearer scheme.
      scheme: bearer
      bearerFormat: JWT
```

Every security should be applied in API operation as needed.
```yaml
security:
  - BearerAuth: []
```

```yaml
paths:
  /signin:
    post: 
      tags:
        - Auth
      security:
        - BearerAuth: []
```


### Parameters
An object to define path item object parameters. the following attributes are required.

| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| name          | string   | A parameter name |
| description   | string   | A description detail of the parameter |
| in            | string   | A location of the parameter. possible values are `query`, `header`, `path`, and `cookie` |
| required      | boolean  | Determines whether this parameter is mandatory |
| schema        | object   | The schema definition of the parameter. The required attribute is `type` |

```yaml
name: id
description: Parameter description
in: path
required: true
schema: 
  type: integer
```

The parameter object could also be defined as component to make it reusable.

```yaml
components:
  parameters:
    EntityId:
      name: id
      description: Parameter description
      in: path
      required: true
      schema: 
        type: integer
```

It can be easily called everywhere as reference.

```yaml
parameters:
  - $ref: '#/components/parameters/EntityId'
```

### RequestBody
A requestBody defines the body request of path item, the operation that uses this is basically `POST` or `PUT` HTTP Method. the following attributes are required

| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| reqired       | boolean  | Determining whether request body is required or not |
| description   | string   | A description detail of the request body |
| content       | object   | Containing a content type and schema of the request body |

```yaml
requestBody:
  required: true
  description: Sign description
  content:
    application/json:
      schema:
        type: object
        required:
          - username
          - password
        properties:
          username: 
            type: string
            description: Username description
            minLength: 4
            maxLength: 100
          password:
            type: string
            format: password
            description: Password description
            minLength: 8
            maxLength: 100
```

#### Content
The request body content should have content-type, ex: `application/json` and schema. for more the datail of the schema is [here](#schemas)

here are the example:

```yaml
content:
  application/json:
    schema:
```
### Responses
Describes a response from an API operation, the defintion is quite the same as how to define request body. every response should define status code, for example: 200, 400, 422, 500 etc.

The example below, I defined as component.

* Base API Response schema

```yaml
BaseApiResponse:
  description: Base api response schema
  required:
    - message
    - data
  properties:
    message:
      type: string
      description: message of response
    data:
      type: object
      description: data returned from api response
```

* Sign In Response Schema

```yaml
SignInResponse:
  description: Sign in response
  required:
    - user
      - name
      - username
    - access_token
    - refresh_token
  
  allOf:
    - $ref: '#/components/schemas/BaseApiResponse'
    - properties:
        data:
          type: object
          properties:
            user:
              type: object
              properties:
                name: 
                  type: string
                  description: the name of the user
                username:
                  type: string
                  description: the username of the user
            access_token: 
              type: string
              description: the auth access token
            refresh_token:
              type: string
              description: the auth refresh token
```


* Sign In API response component
```yaml
responses:
  SignInResponse:
    description: Successfully operated.
    content:
      application/json:
        schema:
          $ref: '#/components/schemas/SignInResponse'
        
        examples:
          success:
            $ref: '#/components/examples/ValidSigninResponse'
```

Lastly, call it in operation response:

```yaml
responses:
  200:
    $ref: '#/components/responses/SignInResponse'
```

Full Example:
```yaml
paths:
  /signin:
    post: 
      tags:
        - Auth
      security:
        - BearerAuth: []
      
      summary: Sign in summary
      description: Sign in description
      operationId: signin
      requestBody:
        $ref: '#/components/requestBodies/SignInBody'
      responses:
        200:
          $ref: '#/components/responses/SignInResponse'
```

### Components
Component is an element to hold various schema for the specification. for example:

```yaml
components:
  securitySchemes:
    BearerAuth: 
      type: http
      description: JWT Authorization header using the Bearer scheme.
      scheme: bearer
      bearerFormat: JWT
  
  parameters:
    PathParameter:
      name: id
      description: Parameter description
      in: path
      required: true
      schema: 
        type: integer
        format: int32
        minimum: 1

  schemas:
    SignIn:
      required:
        - username
        - password
      properties:
        username: 
          type: string
          description: Username description
          minLength: 4
          maxLength: 100
        password:
          type: string
          format: password
          description: Password description
          minLength: 8
          maxLength: 100
```
#### Schemas
Schema is one of a component and an object that create a reusable Schema Object. for this Standard Definition, the following format should be there.

| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| type          | any      | Containing available type, generally it will contain an object (key-value pair) |
| description   | string   | A description detail of the request body |
| required      | array    | Containing required attributes that should be exist |
| properties    | object   | Containing the attribute names. and each attribute name should contain m |


```yaml
schemas:
    SignIn:
      description: Sign in schema description
      required:
        - username
        - password
      properties:
        username: 
          type: string
          description: Username description
          minLength: 4
          maxLength: 100
        password:
          type: string
          format: password
          description: Password description
          minLength: 8
          maxLength: 100
```

### Examples
The example needed when you want show others how they should define an object. DCI OpenAPI Standard Definition requres you to define an example on every definition, such as: request body, response, parameter etc.

There are some required attribues shown below:

| Attributes    | Type     | Description  |
| :------------ |:---------| :------------|
| description   | string   | A description detail of the example |
| value         | any      | the value of the attribute |


Here is the sample definition as component:
```yaml
components:
  examples:
    ValidSigninBody:
      description: The Signin request body of the correct value
      value:
        username: LX$r578I0
        password: hello&win
    
    InvalidSigninBody:
      description: The Signin request body of the incorrect value
      value:
        username: HM5
        password: Xsh
```

How to use it as ref.

```yaml
examples:
  valid: 
    $ref: '#/components/examples/ValidSigninBody'
  invalid: 
    $ref: '#/components/examples/InvalidSigninBody'
```

Full example:

```yaml
requestBodies:
  SignInBody:
    required: true
    description: Sign description
    content:
      application/json:
        schema: 
          $ref: '#/components/schemas/SignIn'
        examples:
          valid: 
            $ref: '#/components/examples/ValidSigninBody'
          invalid: 
            $ref: '#/components/examples/InvalidSigninBody'
```




